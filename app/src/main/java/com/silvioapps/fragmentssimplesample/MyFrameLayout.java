package com.silvioapps.fragmentssimplesample;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Silvio Guedes on 10/07/2016.
 */
public class MyFrameLayout extends Fragment {
    public static final String KEY_FRAGMENT = "KEY_FRAGMENT";
    public static final String TAG_FRAGMENT = "TAG_FRAGMENT";

    private String argument = null;

    public static MyFrameLayout newInstance(String string){
        Log.i("TAG","MyFrameLayout newInstance");

        Bundle bundle = new Bundle();
        bundle.putString(KEY_FRAGMENT,string);

        MyFrameLayout myFrameLayout = new MyFrameLayout();
        myFrameLayout.setArguments(bundle);

        return myFrameLayout;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        Log.i("TAG","MyFrameLayout onAttach");
    }

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);

        Log.i("TAG","MyFrameLayout onCreate");

        argument = (String)getArguments().getSerializable(KEY_FRAGMENT);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        Log.i("TAG","MyFrameLayout onCreateView");

        View view = layoutInflater.inflate(R.layout.my_frame_layout,viewGroup,false);

        if(argument != null){
            if(argument.equals("BLACK")){
                view.setBackgroundColor(Color.BLACK);
            }
            else if(argument.equals("BLUE")){
                view.setBackgroundColor(Color.BLUE);
            }
            else if(argument.equals("GREEN")){
                view.setBackgroundColor(Color.GREEN);
            }
            else if(argument.equals("RED")){
                view.setBackgroundColor(Color.RED);
            }
            else if(argument.equals("WHITE")){
                view.setBackgroundColor(Color.WHITE);
            }
            else if(argument.equals("YELLOW")){
                view.setBackgroundColor(Color.YELLOW);
            }
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle bundle){
        super.onActivityCreated(bundle);

        Log.i("TAG","MyFrameLayout onActivityCreated");
    }

    @Override
    public void onStart(){
        super.onStart();

        Log.i("TAG","MyFrameLayout onStart");
    }

    @Override
    public void onResume(){
        super.onResume();

        Log.i("TAG","MyFrameLayout onResume");
    }

    @Override
    public void onPause(){
        super.onPause();

        Log.i("TAG","MyFrameLayout onPause");
    }

    @Override
    public void onStop(){
        super.onStop();

        Log.i("TAG","MyFrameLayout onStop");
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();

        Log.i("TAG","MyFrameLayout onDestroyView");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        Log.i("TAG","MyFrameLayout onDestroy");
    }

    @Override
    public void onDetach(){
        super.onDetach();

        Log.i("TAG","MyFrameLayout onDetach");
    }
}
