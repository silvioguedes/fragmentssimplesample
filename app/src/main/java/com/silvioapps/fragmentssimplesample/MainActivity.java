package com.silvioapps.fragmentssimplesample;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements MyListFragment.IOnClickList, MyFragment.IOnDoneEditText{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(Object object){
        MyFrameLayout myFrameLayout = MyFrameLayout.newInstance((String)object);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragment, myFrameLayout, MyFrameLayout.TAG_FRAGMENT);
        fragmentTransaction.commit();
    }

    public void onDone(Object object){
        MyFrameLayout myFrameLayout = MyFrameLayout.newInstance((String)object);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragment, myFrameLayout, MyFrameLayout.TAG_FRAGMENT);
        fragmentTransaction.commit();
    }
}
