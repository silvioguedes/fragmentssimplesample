package com.silvioapps.fragmentssimplesample;

import android.app.Activity;
import android.app.Fragment;
import android.app.ListFragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Silvio Guedes on 10/07/2016.
 */
public class MyFragment extends Fragment {
    private EditText editText = null;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        Log.i("TAG","MyFragment onAttach");
    }

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);

        Log.i("TAG","MyFragment onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        Log.i("TAG","MyFragment onCreateView");

        View view = layoutInflater.inflate(R.layout.my_fragment,viewGroup,false);

        editText = (EditText)view.findViewById(R.id.editText);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(i == EditorInfo.IME_ACTION_DONE){

                    Activity activity = getActivity();

                    if(activity instanceof IOnDoneEditText) {
                        IOnDoneEditText onDoneEditText = (IOnDoneEditText) activity;
                        onDoneEditText.onDone(textView.getText().toString());

                        return true;
                    }
                }

                return false;
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle bundle){
        super.onActivityCreated(bundle);

        Log.i("TAG","MyFragment onActivityCreated");


    }

    @Override
    public void onStart(){
        super.onStart();

        Log.i("TAG","MyFragment onStart");
    }

    @Override
    public void onResume(){
        super.onResume();

        Log.i("TAG","MyFragment onResume");
    }

    @Override
    public void onPause(){
        super.onPause();

        Log.i("TAG","MyFragment onPause");
    }

    @Override
    public void onStop(){
        super.onStop();

        Log.i("TAG","MyFragment onStop");
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();

        Log.i("TAG","MyFragment onDestroyView");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        Log.i("TAG","MyFragment onDestroy");
    }

    @Override
    public void onDetach(){
        super.onDetach();

        Log.i("TAG","MyFragment onDetach");
    }

    public interface IOnDoneEditText{
        void onDone(Object object);
    }
}
