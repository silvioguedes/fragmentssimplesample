package com.silvioapps.fragmentssimplesample;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

/**
 * Created by Silvio Guedes on 10/07/2016.
 */
public class MyListFragment extends ListFragment {

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        Log.i("TAG","MyListFragment onAttach");
    }

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);

        Log.i("TAG","MyListFragment onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        Log.i("TAG","MyListFragment onCreateView");

        return super.onCreateView(layoutInflater,viewGroup,bundle);
    }

    @Override
    public void onActivityCreated(Bundle bundle){
        super.onActivityCreated(bundle);

        Log.i("TAG","MyListFragment onActivityCreated");

        ArrayList<String> stringsList = new ArrayList<String>();
        stringsList.clear();
        stringsList.add("BLACK");
        stringsList.add("BLUE");
        stringsList.add("GREEN");
        stringsList.add("RED");
        stringsList.add("WHITE");
        stringsList.add("YELLOW");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, stringsList);
        setListAdapter(arrayAdapter);
    }

    @Override
    public void onStart(){
        super.onStart();

        Log.i("TAG","MyListFragment onStart");
    }

    @Override
    public void onResume(){
        super.onResume();

        Log.i("TAG","MyListFragment onResume");
    }

    @Override
    public void onPause(){
        super.onPause();

        Log.i("TAG","MyListFragment onPause");
    }

    @Override
    public void onStop(){
        super.onStop();

        Log.i("TAG","MyListFragment onStop");
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();

        Log.i("TAG","MyListFragment onDestroyView");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        Log.i("TAG","MyListFragment onDestroy");
    }

    @Override
    public void onDetach(){
        super.onDetach();

        Log.i("TAG","MyListFragment onDetach");
    }

    @Override
    public void onListItemClick(ListView listView,View view,int position,long id){
        super.onListItemClick(listView,view,position,id);

        Log.i("TAG","MyListFragment onListItemClick");

        Activity activity = getActivity();
        if(activity instanceof IOnClickList) {
            String argument = (String)listView.getItemAtPosition(position);

            IOnClickList onClickList = (IOnClickList) activity;
            onClickList.onClick(argument);
        }
    }

    public interface IOnClickList{
        void onClick(Object object);
    }
}
